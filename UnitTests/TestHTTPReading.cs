﻿using System;
using System.Diagnostics;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class TestHTTPReading
    {
        [TestMethod]
        public void TestReadMaleFirstNames()
        {
            using (WebClient client = new WebClient())
            {
                string s = client.DownloadString("http://kiralee.ddns.net/DBMocker/maleFirstNames.txt");
                Console.WriteLine(s);
            }
        }

        [TestMethod]
        public void TestReadFemaleFirstNames()
        {
            using (WebClient client = new WebClient())
            {
                string s = client.DownloadString("http://kiralee.ddns.net/DBMocker/femaleFirstNames.txt");
                Console.WriteLine(s);
            }
        }

        [TestMethod]
        public void TestReadLastNames()
        {
            using (WebClient client = new WebClient())
            {
                string s = client.DownloadString("http://kiralee.ddns.net/DBMocker/lastNames.txt");
                Console.WriteLine(s);
            }
        }
    }
}
