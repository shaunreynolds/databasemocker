﻿using System;
using Common.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class TestExtendedRandom
    {
        [TestMethod]
        public void TestGetChar()
        {
            ExtendedRandom er = new ExtendedRandom();
            Log.WriteLine(er.NextChar());
        }

        [TestMethod]
        public void TestNextIntRange()
        {
            ExtendedRandom er = new ExtendedRandom();
            int temp = er.NextIntRange(10, 20);
                Log.WriteLine("Next int in range of 10, 20 is : " + temp);
                Assert.IsTrue(temp >= 10 && temp <= 20);
        }

        [TestMethod]
        public void TestNextDoubleRange()
        {
            ExtendedRandom er = new ExtendedRandom();
            double temp = er.NextDoubleRange(10.5, 20.5);
                Log.WriteLine("Next double in range of 10.5, 20.5 is : " + temp);
                Assert.IsTrue(temp >= 10.5 && temp <= 20.5);
        }

        [TestMethod]
        public void TestNextIntRangeException()
        {
            ExtendedRandom er = new ExtendedRandom();
            Assert.ThrowsException<ArgumentException>(()=>er.NextIntRange(20, 10));
        }

        [TestMethod]
        public void TestNextDoubleRangeException()
        {
            ExtendedRandom er = new ExtendedRandom();
            Assert.ThrowsException<ArgumentException>(() => er.NextDoubleRange(20.1, 10.9));
        }
    }
}
