﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Common.Utilities
{
    /// <summary>
    /// A static convenience class that adds methods for certain types of <see cref="string"/> manipulation.
    /// </summary>
    static class StringManipulation
    {
        /// <summary>
        /// Converts the supplied <see cref="string"/> to <code>lowerCamelCase</code>.
        /// </summary>
        /// <param name="input">Input string to convert.</param>
        /// <returns>The string converted to lowerCamelCase.</returns>
        public static string ToLowerCamelCase(string input)
        {
            if (input.Length <= 0)
            {
                return input;
            }
            string[] words = input.Split(' ');
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < words.Length; i++)
            {
                string word = words[i];
                if (i == 0)
                {
                    word = Char.ToLowerInvariant(word[0]) + word.Substring(1);
                }
                sb.Append(word);
            }
            return sb.ToString();
        }

        /// <summary>
        /// Trims the amount of characters specified from the end of string and returns the result.
        /// </summary>
        /// <param name="input">The string to trim.</param>
        /// <param name="numChars">The amount of chars to trim from the end.</param>
        /// <returns>A trimmed string.</returns>
        public static string TrimFromEnd(string input, int numChars)
        {
            if (input.Length < numChars)
            {
                return input;
            }
            return input.Substring(0,input.Length - numChars);
        }

        /// <summary>
        /// Reads the supplied URL as a series of <see cref="string"/>s and returns them as a fixed length array.
        /// </summary>
        /// <param name="url">The URL to read from.</param>
        /// <returns>A fixed length array of strings.</returns>
        public static string[] GetStringArrayFromURL(string url)
        {
            using (WebClient client = new WebClient())
            {
                List<string> names = new List<string>();
                var stream = client.OpenRead(url);
                var reader = new StreamReader(stream);
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    names.Add(line);
                }
                reader.Close();
                reader.Dispose();
                stream.Flush();
                stream.Close();
                stream.Dispose();
                return names.ToArray();
            }
        }
    }
}
