﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace Common.Utilities
{
    /// <summary>
    /// A simple re-invention of the wheel to enable logging throughout an application.
    /// Exposes <code>WriteLine()</code> methods that take various arguments, akin to
    /// those provided by a <see cref="TextWriter"/>.
    /// These messages are written to a list of TextWriters, to which you can add your own.
    /// By default, this class adds <see cref="Console.Out"/> and a <see cref="StreamWriter"/>
    /// set to create a file called debug.txt to the list of available TextWriters.
    /// Remember to call <see cref="Dispose"/> at the end of your application to let
    /// this class flush and dispose all TextWriters.
    /// </summary>
    public static class Log
    {
        private static readonly List<string> CURRENT_LOG = new List<string>();
        private static readonly List<TextWriter> OUTPUTS = new List<TextWriter>();

        static Log()
        {
            AddTextWriter(Console.Out);
            AddTextWriter(new StreamWriter("debug.txt"));
        }

        /// <summary>
        /// Adds a <see cref="TextWriter"/> to the list of available TextWriters that are used for output.
        /// By default this class will back-fill the TextWriter with the current log to bring it
        /// up to speed as it were, this can be disabled with the optional parameter, <code>appendCurrentLog</code>.
        /// </summary>
        /// <param name="tw">The TextWriter to add to the list.</param>
        /// <param name="appendCurrentLog">Whether or not to back fill the supplied TextWriter with the current log.</param>
        public static void AddTextWriter(TextWriter tw,bool appendCurrentLog = true)
        {
            OUTPUTS.Add(tw);
            if (appendCurrentLog)
            {
                foreach(string s in CURRENT_LOG)
                {
                    tw.WriteLine(s);
                }
            }
        }

        /// <summary>
        /// Appends a time stamp, calling class name, and calling method name to the given <see cref="string"/> 
        /// then outputs the result to all <see cref="TextWriter"/>s in the list.
        /// </summary>
        /// <param name="s">The string to output.</param>
        public static void WriteLine(string s)
        {
            // Walk up the call stack to find which class called this method
            int callLevel = 0;
            StackFrame frame = new StackFrame(callLevel);
            string className = frame.GetMethod().DeclaringType.Name;
            string methodName = "";
            while(className == "Log")
            {
                callLevel++;
                frame = new StackFrame(callLevel);
                className = frame.GetMethod().DeclaringType.Name;
                methodName = frame.GetMethod().Name;
            }

            // Grab a time stamp
            string timeStamp = DateTime.Now.ToString();

            string finalString = $"[{timeStamp}] : [{className}.{methodName}] : {s}";
            CURRENT_LOG.Add(finalString);
            foreach(TextWriter tw in OUTPUTS)
            {
                tw.WriteLine(finalString);
            }
        }

        /// <summary>
        /// Convenienve method to output the supplied <see cref="object"/>. Simply calls <code>WriteLine(o.ToString())</code>.
        /// </summary>
        /// <param name="o">The object to output.</param>
        public static void WriteLine(object o)
        {
            WriteLine(o.ToString());
        }

        /// <summary>
        /// Ensures all underlying <see cref="TextWriter"/>s are flushed and disposed.
        /// Not calling this method will most likely result in memory leaks.
        /// </summary>
        public static void Dispose()
        {
            foreach(TextWriter tw in OUTPUTS)
            {
                tw.Flush();
                tw.Close();
                tw.Dispose();
            }
        }
    }
}
