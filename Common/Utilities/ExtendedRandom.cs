﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Utilities
{
    /// <summary>
    /// An extension of the built in <see cref="Random"/> class, with a few extra methods for randomly
    /// generating various types of primitives.
    /// </summary>
    public class ExtendedRandom : Random
    {
        private char[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();

        /// <summary>
        /// Public constructo that calls <code>base(Guid.NewGuid().GetHashCode())</code> to ensure all instances are truly random.
        /// </summary>
        public ExtendedRandom() : base(Guid.NewGuid().GetHashCode())
        {

        }
        
        /// <summary>
        /// Returns a random <see cref="char"/> from the alphabet, capitalised.
        /// Uses the standard Latin alphabet.
        /// </summary>
        /// <returns>A random char.</returns>
        public char NextChar()
        {
            return alphabet[this.Next(alphabet.Length)];
        }

        /// <summary>
        /// Returns a random <see cref="bool"/>.
        /// </summary>
        /// <returns>A boolean.</returns>
        public bool NextBool()
        {
            return this.NextDouble() <= 0.5;
        }

        /// <summary>
        /// Returns a random <see cref="DateTime"/> that falls between min and max.
        /// </summary>
        /// <param name="min">The minimum date.</param>
        /// <param name="max">The maximum date.</param>
        /// <returns>A DateTime.</returns>
        public DateTime NextDateTimeRange(DateTime min, DateTime max)
        {
            int range = (max - min).Days;
            return min.AddDays(Next(range));
        }

        /// <summary>
        /// Returns an <see cref="int"/> within a range, inclusively.
        /// </summary>
        /// <param name="min">Minimum number for the result to be.</param>
        /// <param name="max">Maximum number for the result to be.</param>
        /// <returns>A randomly generated int between min and max.</returns>
        /// /// <exception cref="ArgumentException">Thrown if max is less than or equal to min.</exception>
        public int NextIntRange(int min, int max)
        {
            if (max <= min)
            {
                throw new ArgumentException("Max cannot be less than or equal to min.");
            }
            return (int)this.NextDoubleRange(min, max);
        }

        /// <summary>
        /// Returns a <see cref="double"/> within a range, inclusively.
        /// </summary>
        /// <param name="min">Minimum number for the result to be.</param>
        /// <param name="max">Maximum number for the result to be.</param>
        /// <returns>A randomly generated double between min and max.</returns>
        /// <exception cref="ArgumentException">Thrown if max is less than or equal to min.</exception>
        public double NextDoubleRange(double min, double max)
        {
            if (max <= min)
            {
                throw new ArgumentException("Max cannot be less than or equal to min.");
            }
            double range = max - min;
            double result = min + this.NextDouble() * range;
            return result;
        }
    }
}
