﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Utilities
{
    /// <summary>
    /// Convenience helper class for shuffling a <see cref="List{T}"/> of objects.
    /// </summary>
    public static class ArrayManipulation
    {
        /// <summary>
        /// This method has been taken directly from <see href="https://stackoverflow.com/a/1262619">this stackoverflow answer</see>,
        /// with the addition of using my own <see cref="ExtendedRandom"/> instead of the default <see cref="Random"/>.
        /// </summary>
        /// <typeparam name="T">Type of objects in the list.</typeparam>
        /// <param name="list">The list to shuffle.</param>
        public static void Shuffle<T>(this IList<T> list)
        {
            ExtendedRandom rng = new ExtendedRandom();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static T[] ConcatArrays<T>(T[] firstArray, T[] secondArray)
        {
            List<T> temp = new List<T>(firstArray.Concat<T>(secondArray));
            return temp.ToArray();
        }
    }
}
