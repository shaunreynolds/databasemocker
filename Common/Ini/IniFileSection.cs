﻿using Common.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.Ini
{
    public class IniFileSection
    {
        private readonly Dictionary<string, string> keyValues = new Dictionary<string, string>();
        private string name;

        public IniFileSection(string name)
        {
            this.name = name;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public void WriteTo(TextWriter tw)
        {
            tw.WriteLine($"[{name}]");
            foreach(KeyValuePair<string,string> kvp in keyValues)
            {
                tw.WriteLine($"{kvp.Key}={kvp.Value}");
            }
        }

        public void SetKVP(string key, string value)
        {
            if (keyValues.ContainsKey(key))
            {
                keyValues[key] = value;
            }
            else
            {
                keyValues.Add(key, value);
            }
        }

        public string GetName()
        {
            return name;
        }

        public string GetString(string key, object defaultValue=null)
        {
            if (!keyValues.ContainsKey(key))
            {
                keyValues.Add(key, defaultValue.ToString());
            }
            return keyValues[key];
        }

        public int GetInt(string key, int defaultValue = int.MaxValue)
        {
            try
            {
                return int.Parse(GetString(key,defaultValue));
            }
            catch
            {
                Log.WriteLine("Cannot parse as an int, returning default value of :" + defaultValue);
                return defaultValue;
            }
        }

        public double GetDouble(string key, double defaultValue = double.MaxValue)
        {
            try
            {
                return double.Parse(GetString(key,defaultValue));
            }
            catch
            {
                Log.WriteLine("Cannot parse as a double, returning default value of :" + defaultValue);
                return defaultValue;
            }
        }

        public bool GetBool(string key, bool defaultValue = true)
        {
            try
            {
                return bool.Parse(GetString(key,defaultValue));
            }
            catch
            {
                Log.WriteLine("Cannot parse as a bool, returning default value of :" + defaultValue);
                return defaultValue;
            }
        }

        public bool ContainsKey(string key)
        {
            return keyValues.ContainsKey(key);
        }

        public override string ToString()
        {
            string result = "";
            result += "Name: " + name + " KVP: ";
            foreach(var kvp in keyValues)
            {
                result += $" key:{kvp.Key}={kvp.Value}";
            }
            return result;
        }
    }
}
