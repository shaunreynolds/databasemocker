﻿using Common.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Common.Ini
{
    public class IniFile
    {
        private readonly Dictionary<string, IniFileSection> sections = new Dictionary<string, IniFileSection>();
        private readonly string filePath;

        public IniFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }
            ReadFrom(filePath);
            this.filePath = filePath;
        }

        public void Save()
        {
            if (filePath == null)
            {
                Log.WriteLine("Cannot save an ini file that wasn't initially read. Using WriteTo instead.");
            }
            else
            {
                WriteTo(filePath);
            }
        }

        public IniFileSection GetIniFileSection(string name)
        {
            if (!sections.ContainsKey(name))
            {
                sections.Add(name, new IniFileSection(name));
            }
            return sections[name];
        }

        public void AddIniFileSection(IniFileSection ifs)
        {
            if (!sections.ContainsKey(ifs.GetName()))
            {
                sections.Add(ifs.GetName(), ifs);
            }
            else
            {
                sections[ifs.GetName()] = ifs;
            }
        }

        public void ReadFrom(string filePath)
        {
            IniFileSection current=new IniFileSection("");
            StreamReader sr = new StreamReader(filePath);
            while (sr.Peek() != -1)
            {
                string line = sr.ReadLine();
                if (line.StartsWith("#") || line.StartsWith(" ") || line.Length == 0)
                {
                    continue;
                }

                if (line.StartsWith("["))
                {
                    line = line.Replace("[", "");
                    line = line.Replace("]", "");
                    current = new IniFileSection(line);
                    sections.Add(current.GetName(), current);
                }
                if (line.Contains("="))
                {
                    string[] kvp = line.Split('=');
                    current.SetKVP(kvp[0], kvp[1]);
                }
            }

            sr.Close();
            sr.Dispose();
        }

        public void WriteTo(string filePath)
        {
            StreamWriter sw = new StreamWriter(filePath);
            foreach(IniFileSection ifs in sections.Values)
            {
                ifs.WriteTo(sw);
            }
            sw.Flush();
            sw.Close();
            sw.Dispose();
        }
    }
}
