﻿using Common.Utilities;
using DatabaseMocker.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    /// <summary>
    /// Generates data values that already exist in another <see cref="Table"/>'s <see cref="Column"/>.
    /// </summary>
    public class ColumnDataGenerator : AbstractGenerator
    {
        private Column column;
        private Table table;
        public ColumnDataGenerator():base("columnDataGenerator","Column Data Generator")
        {

        }

        public override GeneratorCategory GetCategory()
        {
            return GeneratorCategory.Data;
        }

        public override GeneratorOptions GetGeneratorOptions()
        {
            if (generatorOptions.IsEmpty())
            {
                GeneratorOptions go = new GeneratorOptions();
                go.AddGeneratorOption("table", "Table name: ", "");
                go.AddGeneratorOption("column", "Column name: ", "");
                SetGeneratorOptions(go);
                return go;
            }
            else
            {
                return generatorOptions;
            }
        }

        public override string GetData()
        {
            try
            {
                if (column == null)
                {
                    string tableName = generatorOptions.GetString("table");
                    string columnName = generatorOptions.GetString("column");
                    column = Tables.GetTable(tableName).GetColumn(columnName);
                    table = Tables.GetTable(tableName);
                }
                if (!table.IsGenerated())
                {
                    table.Generate();
                }
                return column.GetDataset()[extendedRandom.Next(column.GetDataset().Length)];
            }
            catch
            {
                Log.WriteLine("Cannot find table or column specified, returning empty string instead");
                return "";
            }
        }

        public override bool RequiresForeignKey()
        {
            return true;
        }

        public override string GetDataType()
        {
            if (column == null)
            {
                string tableName = generatorOptions.GetString("table");
                string columnName = generatorOptions.GetString("column");
                column = Tables.GetTable(tableName).GetColumn(columnName);
                table = Tables.GetTable(tableName);
            }
            string result = column.GetDataType();
            Log.WriteLine("Column datatype is this: " + result);
            if (result.Contains("AUTO_INCREMENT"))
            {
                Log.WriteLine("Datatype contains auto_increment so replacing it with blank space");
                result=result.Replace("AUTO_INCREMENT","");
            }
            Log.WriteLine(result);
            return result;

        }
    }
}
