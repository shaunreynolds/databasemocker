﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    /// <summary>
    /// This generator randomly generates email addresses.
    /// </summary>
    public class EmailGenerator : AbstractGenerator
    {
        private readonly string[] firstNames;
        private readonly string[] lastNames;
        private readonly string[] emailProviders;
        private readonly string[] domains;
        public EmailGenerator() : base("emailGenerator", "Email Generator")
        {
            firstNames = Data.Data.ONLINE_LISTS["all_names"];
            lastNames = Data.Data.ONLINE_LISTS["last_names"];
            emailProviders = Data.Data.EMAIL_PROVIDERS;
            domains = Data.Data.DOMAIN_EXTENSIONS;
        }
        public override GeneratorCategory GetCategory()
        {
            return GeneratorCategory.Personal;
        }

        public override string GetData()
        {
            string fName = firstNames[extendedRandom.Next(firstNames.Length)];
            string lName = lastNames[extendedRandom.Next(lastNames.Length)];
            string email = emailProviders[extendedRandom.Next(emailProviders.Length)];
            string domain = domains[extendedRandom.Next(domains.Length)];
            char seperator = extendedRandom.NextBool() ? '-' : '_';

            return $"{fName}{seperator}{lName}@{email}.{domain}";
        }

        public override string GetDataType()
        {
            return "VARCHAR(255)";
        }
    }
}
