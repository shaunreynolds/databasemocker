﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    /// <summary>
    /// Randomly generates <see cref="DateTime"/> strings that fall between the minimum and maximum DateTimes given in the GeneratorOptions.
    /// </summary>
    public class DateTimeGenerator : AbstractGenerator
    {
        public DateTimeGenerator(): base("dateTimeGenerator","DateTime Generator")
        {

        }

        public override GeneratorOptions GetGeneratorOptions()
        {
            if (generatorOptions.IsEmpty())
            {
                GeneratorOptions go = new GeneratorOptions();
                go.AddGeneratorOption("min", "Minimum date: ", DateTime.Today);
                go.AddGeneratorOption("max", "Maximum date: ", DateTime.Today.AddDays(1));
                SetGeneratorOptions(go);
                return go;
            }
            else
            {
                return generatorOptions;
            }
        }

        public override GeneratorCategory GetCategory()
        {
            return GeneratorCategory.Numbers;
        }

        public override string GetData()
        {
            DateTime min = GetGeneratorOptions().GetDateTime("min");
            DateTime max = GetGeneratorOptions().GetDateTime("max");
            return extendedRandom.NextDateTimeRange(min, max).ToString("yyyy-MM-dd");
        }

        public override string GetDataType()
        {
            return "DATE";
        }
    }
}
