﻿using Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{

    public class GeneratorOptions
    {
        private readonly Dictionary<string, GeneratorOption> options = new Dictionary<string, GeneratorOption>();
        public static readonly GeneratorOptions EMPTY = new GeneratorOptions();

        public void AddGeneratorOption(string name,string label, object value, bool isEditable = true)
        {
            GeneratorOption go = new GeneratorOption(name, label, value,isEditable);
            if (options.ContainsKey(name))
            {
                Log.WriteLine("Option by name: " + name + " already exists, updating it with label: " + label + " and value: " + value);
                options[name] = go;
            }
            else
            {
                options.Add(name, go);
            }
        }

        public GeneratorOption[] GetAllOptions()
        {
            return options.Values.ToArray();
        }

        /// <summary>
        /// Creates a prepopulated GeneratorOptions object from a string, which itself was created from a GeneratorOptions object.
        /// </summary>
        /// <param name="s">The input string to parse.</param>
        /// <returns>A GeneratorOptions object.</returns>
        public static GeneratorOptions FromString(string s)
        {
            GeneratorOptions result = new GeneratorOptions();
            try
            {
                string[] objects = s.Split('|');
                foreach(string o in objects)
                {
                    string[] values = o.Split(';');
                    result.AddGeneratorOption(values[0], values[1], values[2],bool.Parse(values[3]));
                }
            }catch (Exception e)
            {
                Log.WriteLine("Could not parse GeneratorOptions object from input string: " + s + ", caught exception: " + e+" returning empty GeneratorOptions instead.");
            }
            return result;
        }

        public bool ContainsKey(string key)
        {
            return options.ContainsKey(key);
        }

        public object GetOption(string name)
        {
            if (options.ContainsKey(name))
            {
                Log.WriteLine($"Found option matching name: {name} returning value for that option.");
                return options[name].GetValue();
            }
            else
            {
                Log.WriteLine($"No option found matching name: {name} returning value of null.");
                return null;
            }
        }

        public string GetLabel(string name)
        {
            if (options.ContainsKey(name))
            {
                Log.WriteLine($"Found option matching name: {name} returning value for that option.");
                return options[name].GetLabel();
            }
            else
            {
                Log.WriteLine($"No option found matching name: {name} returning empty string instead.");
                return "";
            }
        }

        public override string ToString()
        {
            string result = "";
            foreach(KeyValuePair<string,GeneratorOption> kvp in options)
            {
                result += $"{kvp.Key};{kvp.Value.GetLabel()};{kvp.Value.GetValue()};{kvp.Value.IsEditable().ToString()}|";
            }
            result= StringManipulation.TrimFromEnd(result, 1);
            return result;
        }

        public bool IsEmpty()
        {
            Log.WriteLine($"Count of options in this GeneratorOptions object: {options.Count}");
            return (options.Count == 0);
        }

        public int GetInt(string name)
        {
            return int.Parse(options[name].GetValue().ToString());
        }

        public DateTime GetDateTime(string name)
        {
            return Convert.ToDateTime(options[name].GetValue().ToString());
        }

        public float GetFloat (string name)
        {
            return float.Parse(options[name].GetValue().ToString());
        }

        public double GetDouble(string name)
        {
            return double.Parse(options[name].GetValue().ToString());
        }

        public string GetString(string name)
        {
            return options[name].GetValue().ToString();
        }

        public string[] GetStrings(string name)
        {
            return GetString(name).Split(',');
        }

        public class GeneratorOption
        {
            private readonly string name;
            private readonly string label;
            private readonly object value;
            private readonly bool isEditable;

            public GeneratorOption(string name, string label,object value, bool isEditable)
            {
                this.name = name;
                this.label = label;
                this.value = value;
                this.isEditable = isEditable;
            }

            public object GetValue()
            {
                return value;
            }

            public string GetName()
            {
                return name;
            }

            public string GetLabel()
            {
                return label;
            }

            public bool IsEditable()
            {
                return isEditable;
            }
        }
    }
}
