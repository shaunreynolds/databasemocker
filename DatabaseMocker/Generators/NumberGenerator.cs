﻿using Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    /// <summary>
    /// Class for randomly generating numbers.
    /// </summary>
    public class NumberGenerator : AbstractGenerator
    {
        public NumberGenerator():base("numberGenerator","Number Generator")
        {

        }

        public override GeneratorOptions GetGeneratorOptions()
        {
            Log.WriteLine("GetGeneratorOptions is being called from correct source");
            if(generatorOptions.IsEmpty())
            {
                GeneratorOptions go = new GeneratorOptions();
                go.AddGeneratorOption("decimalPlaces", "Number of decimal places:", 0);
                go.AddGeneratorOption("min", "Minimum value:", 0.0);
                go.AddGeneratorOption("max", "Maximum value:", 10.0);
                SetGeneratorOptions(go);
                return go;
            }
            else
            {
                return generatorOptions;
            }
        }

        public override GeneratorCategory GetCategory()
        {
            return GeneratorCategory.Numbers;
        }

        public override string GetData()
        {
            int decimalPlaces = GetGeneratorOptions().GetInt("decimalPlaces");
            double min = GetGeneratorOptions().GetFloat("min");
            double max = GetGeneratorOptions().GetFloat("max");
            double result = extendedRandom.NextDoubleRange(min, max);

            return Math.Round(result, decimalPlaces).ToString();
        }

        public override string GetDataType()
        {
            if (GetGeneratorOptions().GetInt("decimalPlaces") == 0)
            {
                return "INT";
            }
            else
            {
                return "FLOAT";
            }
        }
    }
}
