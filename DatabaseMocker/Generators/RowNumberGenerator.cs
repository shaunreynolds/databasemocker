﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    public class RowNumberGenerator : AbstractGenerator
    {
        private int i = 0;
        public RowNumberGenerator() : base("rowNumberGenerator", "Row Number Generator")
        {

        }
        public override GeneratorCategory GetCategory()
        {
            return GeneratorCategory.Numbers;
        }

        public override string GetData()
        {
            i++;
            return i.ToString();
        }

        public override string GetDataType()
        {
            return "INT AUTO_INCREMENT";
        }
    }
}
