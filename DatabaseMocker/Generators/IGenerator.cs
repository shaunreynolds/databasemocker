﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    /// <summary>
    /// The IGenerator interface defines how Generators are to behave.
    /// </summary>
    public interface IGenerator
    {
        /// <summary>
        /// Get the randomly generated data value that this generator generates.
        /// </summary>
        /// <returns>A string of random data.</returns>
        string GetData();

        /// <summary>
        /// Gets the MySQL datatype associated with this generator. 
        /// </summary>
        /// <returns>A string of a MySQL datatype.</returns>
        string GetDataType();

        /// <summary>
        /// Gets the GeneratorOptions this generator is using to determine it's behaviour.
        /// </summary>
        /// <returns>A GeneratorOptions object. If one hasn't been previously set, this will be default options for this generator.</returns>
        GeneratorOptions GetGeneratorOptions();

        /// <summary>
        /// Sets the GeneratorOptions for this generator.
        /// </summary>
        /// <param name="go">The GeneratorOptions object for this generator to use.</param>
        void SetGeneratorOptions(GeneratorOptions go);

        /// <summary>
        /// Gets the name of this generator to be used when saving to disk.
        /// </summary>
        /// <returns>A string representing the 'code' name of this generator.</returns>
        string GetName();

        /// <summary>
        /// Gets a name for this generator that will make sense to a human reading it.
        /// </summary>
        /// <returns>A string representing a name that humans will understand.</returns>
        string GetHumanReadableName();

        /// <summary>
        /// Returns the category this generator belongs to.
        /// </summary>
        /// <returns></returns>
        GeneratorCategory GetCategory();
    }
}
