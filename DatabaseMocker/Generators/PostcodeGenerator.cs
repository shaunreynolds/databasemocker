﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    /// <summary>
    /// Generates UK postal codes.
    /// </summary>
    public class PostcodeGenerator : AbstractGenerator
    {
        public PostcodeGenerator():base("postcodeGenerator","Post Code Generator")
        {

        }

        public override GeneratorCategory GetCategory()
        {
            return GeneratorCategory.Address;
        }

        public override string GetData()
        {
            char c1 = extendedRandom.NextChar();
            char c2 = extendedRandom.NextChar();
            int i1 = extendedRandom.Next(10);
            int i2 = extendedRandom.Next(10);
            int i3 = extendedRandom.Next(10);
            char c3 = extendedRandom.NextChar();
            char c4 = extendedRandom.NextChar();

            return "" + c1 + c2 + i1 + i2 + i3 + c3 + c4;
;        }

        public override string GetDataType()
        {
            return "VARCHAR (255)";
        }
    }
}
