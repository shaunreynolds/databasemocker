﻿using Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    /// <summary>
    /// A class for randomly selecting data from a list.
    /// </summary>
    public class ListGenerator : AbstractGenerator
    {
        public ListGenerator():base("listGenerator","List Generator")
        {

        }
        public override GeneratorCategory GetCategory()
        {
            return GeneratorCategory.List;
        }

        public override GeneratorOptions GetGeneratorOptions()
        {
            if (generatorOptions.IsEmpty())
            {
                GeneratorOptions go = new GeneratorOptions();
                string temp = "";
                foreach(string s in Data.Data.ONLINE_LISTS.Keys)
                {
                    temp += s + ",";
                }
                temp = StringManipulation.TrimFromEnd(temp, 1);
                go.AddGeneratorOption("lists", "Available lists: ",temp,false);
                go.AddGeneratorOption("listName", "Selected list: ", "nouns");
                SetGeneratorOptions(go);
                return go;
            }
            else
            {
                return generatorOptions;
            }
        }

        public override string GetData()
        {
            string listName = GetGeneratorOptions().GetString("listName");
            string[] list = Data.Data.ONLINE_LISTS[listName];
            return list[extendedRandom.Next(list.Length)];
        }

        public override string GetDataType()
        {
            return "VARCHAR(255)";
        }
    }
}
