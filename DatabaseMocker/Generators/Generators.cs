﻿using Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    public static class Generators
    {
        private static readonly Dictionary<string, string> registeredGenerators = new Dictionary<string, string>();

        public static void RegisterGenerator(string name, Type generator)
        {
            Log.WriteLine($"Attempting to register generator with name: {name} and full assembly name: {generator.FullName}");
            if (registeredGenerators.ContainsKey(name))
            {
                registeredGenerators[name] = generator.FullName;
            }
            else
            {
                registeredGenerators.Add(name, generator.FullName);
            }
        }

        static Generators()
        {
            RegisterGenerator(new EmailGenerator(), new ListGenerator(), new NumberGenerator(), new PostcodeGenerator(), new RowNumberGenerator(), new DateTimeGenerator(), new ColumnDataGenerator());
        }

        public static AbstractGenerator[] GetGenerators()
        {
            List<AbstractGenerator> result = new List<AbstractGenerator>();
            foreach(string s in registeredGenerators.Keys)
            {
                result.Add(GetGeneratorByName(s));
            }
            return result.ToArray();
        }

        private static void RegisterGenerator(params AbstractGenerator[] generators)
        {
            foreach(AbstractGenerator g in generators)
            {
                RegisterGenerator(g.GetName(), g.GetType());
            }
        }

        public static AbstractGenerator GetGeneratorByName(string name)
        {
            Type type = Type.GetType(registeredGenerators[name], true);
            return (AbstractGenerator)Activator.CreateInstance(type);
        }
    }
}
