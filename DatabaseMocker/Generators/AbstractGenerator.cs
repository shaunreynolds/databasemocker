﻿using Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Generators
{
    /// <summary>
    /// An abstract class that implements the IGenerator interface and handles some boilerplate code for future generators to use.
    /// </summary>
    public abstract class AbstractGenerator : IGenerator
    {
        private readonly string name;
        private readonly string humanName;
        protected GeneratorOptions generatorOptions;
        protected readonly ExtendedRandom extendedRandom;

        public AbstractGenerator(string name, string humanName)
        {
            this.name = name;
            this.humanName = humanName;
            this.extendedRandom = new ExtendedRandom();
            this.generatorOptions = GeneratorOptions.EMPTY;
        }

        public abstract GeneratorCategory GetCategory();

        public abstract string GetData();

        public abstract string GetDataType();

        public virtual GeneratorOptions GetGeneratorOptions()
        {
            return generatorOptions;
        }

        public virtual bool RequiresForeignKey()
        {
            return false;
        }

        public string GetHumanReadableName()
        {
            return humanName;
        }

        public string GetName()
        {
            return name;
        }

        public void SetGeneratorOptions(GeneratorOptions go)
        {
            this.generatorOptions = go;
        }

        public override string ToString()
        {
            return humanName;
        }
    }
}
