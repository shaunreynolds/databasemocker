﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Database
{
    /// <summary>
    /// Keeps a list of <see cref="Table"/>s statically.
    /// </summary>
    public static class Tables
    {
        private static readonly Dictionary<string, Table> tables = new Dictionary<string, Table>();

        public static void AddTable(Table t)
        {
            if (tables.ContainsKey(t.GetName()))
            {
                tables[t.GetName()] = t;
            }
            else
            {
                tables.Add(t.GetName(), t);
            }
        }

        public static Table GetTable(string name)
        {
            return tables[name];
        }
    }
}
