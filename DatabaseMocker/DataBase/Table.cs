﻿using Common.Ini;
using Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Database
{
    /// <summary>
    /// Represents a database table. Contains a dictionary of columns mapped to their name, and handles generating the data.
    /// </summary>
    public class Table
    {
        private string name;
        private readonly Dictionary<string, Column> columns = new Dictionary<string, Column>();
        private int numRows;

        public Table(string name, int numRows)
        {
            this.name = name;
            this.numRows = numRows;
        }

        public void SetName(string name)
        {
            this.name = name;
        }
        public void SetNumRows(int numRows)
        {
            this.numRows = numRows;
        }

        public static Table CreateFrom(IniFileSection ifs)
        {
            string name = ifs.GetString("name");
            int numRows = ifs.GetInt("numRows");
            return new Table(name, numRows);
        }

        public void RemoveColumn(string name)
        {
            columns.Remove(name);
        }

        public IniFileSection GetIniFileSection()
        {
            IniFileSection ifs = new IniFileSection($"Table_{name}");
            ifs.SetKVP("name", name);
            ifs.SetKVP("numRows", numRows.ToString());
            string temp = "";
            foreach(Column c in columns.Values)
            {
                temp += c.GetName() + ",";
            }
            temp = StringManipulation.TrimFromEnd(temp, 1);
            ifs.SetKVP("columns", temp);
            return ifs;
        }

        public IniFileSection[] GetColumnIniFileSections()
        {
            List<IniFileSection> ifs = new List<IniFileSection>();
            foreach(Column c in columns.Values)
            {
                IniFileSection temp = c.GetIniFileSection();
                temp.SetName(name +"_" +temp.GetName());
                ifs.Add(temp);
            }
            return ifs.ToArray();
        }

        public string GetName()
        {
            return name;
        }

        public int GetNumRows()
        {
            return numRows;
        }

        public Column GetColumn(string name)
        {
            return columns[name];
        }

        public ICollection<Column> GetColumns()
        {
            return columns.Values;
        }

        public bool IsGenerated()
        {
            foreach(Column c in columns.Values)
            {
                if (!c.IsGenerated())
                {
                    return false;
                }
            }
            return true;
        }

        public void AddColumns(params Column[] c)
        {
            foreach(Column col in c)
            {
                AddColumn(col);
            }
        }

        public void AddColumn(Column c)
        {
            if (columns.ContainsKey(c.GetName()))
            {
                columns[c.GetName()] = c;
            }
            else
            {
                columns.Add(c.GetName(), c);
            }
        }

        public void Generate()
        {
            foreach(Column c in columns.Values)
            {
                c.Generate(numRows);
            }
        }

        public string[] GetInsertStatements()
        {
            if (!IsGenerated())
            {
                Generate();
            }

            string insertString = "INSERT INTO "+name+" (";
            foreach(Column c in columns.Values)
            {
                insertString += c.GetName() + ",";
            }
            insertString = StringManipulation.TrimFromEnd(insertString, 1);
            insertString += ") VALUES (";

            List<string> temp = new List<string>();

            for(int i = 0; i < numRows; i++)
            {
                string tempString = "";
                foreach(Column c in columns.Values)
                {
                    tempString += "'"+c.GetDataset()[i] + "',";
                }
                tempString = StringManipulation.TrimFromEnd(tempString, 1);
                tempString += ");";
                temp.Add(insertString + tempString);
            }

            return temp.ToArray();
        }

        public string[] GetCreateStatement()
        {
            List<string> temp = new List<string>();

            temp.Add("DROP TABLE " + name + ";");
            temp.Add("CREATE TABLE " + name + " (");

            foreach(Column c in columns.Values)
            {
                temp.Add(c.GetName() + " " + c.GetDataType() + ",");
            }

            foreach(Column c in columns.Values)
            {
                if (c.HasForeignKey())
                {
                    temp.Add($"FOREIGN KEY ({c.GetName()}) REFERENCES {c.GetForeignTable().GetName()}({c.GetForeignColumn().GetName()}),");
                }
            }

            foreach(Column c in columns.Values)
            {
                if (c.IsPrimaryKey())
                {
                    temp.Add("PRIMARY KEY (" + c.GetName() +")");
                }
            }

            temp.Add(");");
            return temp.ToArray();
        }
    }
}
