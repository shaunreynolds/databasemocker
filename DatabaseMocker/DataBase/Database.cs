﻿using Common.Ini;
using Common.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Database
{
    /// <summary>
    /// Represents a MySQL database.
    /// </summary>
    public class Database
    {
        private readonly Dictionary<string, Table> tables = new Dictionary<string, Table>();
        private string name;

        public Database(string name)
        {
            this.name = name;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public string GetName()
        {
            return name;
        }

        public Table[] GetTables()
        {
            return tables.Values.ToArray();
        }

        public Table GetTable(string name)
        {
            return tables[name];
        }

        public void WriteOut(TextWriter tw)
        {
            tw.WriteLine("DROP DATABASE " + name+";");
            tw.WriteLine("CREATE DATABASE " + name+";");
            tw.WriteLine("USE " + name+";");
            foreach(Table t in tables.Values)
            {
                tw.WriteLine();
                foreach(string s in t.GetCreateStatement())
                {
                    tw.WriteLine(s);
                }
            }
            foreach(Table t in tables.Values)
            {
                tw.WriteLine();
                foreach(string s in t.GetInsertStatements())
                {
                    tw.WriteLine(s);
                }
            }
        }

        public void AddTables(params Table[] t)
        {
            foreach(Table tbl in t)
            {
                AddTable(tbl);
            }
        }

        public void AddTable(Table t)
        {
            if (tables.ContainsKey(t.GetName()))
            {
                tables[t.GetName()] = t;
            }
            else
            {
                tables.Add(t.GetName(), t);
            }
            Tables.AddTable(t);
        }

        public IniFileSection GetIniFileSection()
        {
            IniFileSection ifs = new IniFileSection("Database");
            ifs.SetKVP("name", name);
            string temp = "";
            foreach(Table t in tables.Values)
            {
                temp += t.GetName() + ",";
            }
            temp = StringManipulation.TrimFromEnd(temp, 1);
            ifs.SetKVP("tables", temp);
            return ifs;
        }

        public void WriteTo(IniFile iF)
        {
            iF.AddIniFileSection(GetIniFileSection());
            foreach(Table t in tables.Values)
            {
                iF.AddIniFileSection(t.GetIniFileSection());
                foreach(IniFileSection ifs in t.GetColumnIniFileSections())
                {
                    iF.AddIniFileSection(ifs);
                }
            }
            iF.Save();
        }

        public void RemoveTable(string name)
        {
            tables.Remove(name);
        }

        public static Database ReadFrom(IniFile file)
        {
            IniFileSection root = file.GetIniFileSection("Database");
            Log.WriteLine("Contents of root: " + root);
            string name = root.GetString("name");

            Database result = new Database(name);
            string[] tables = root.GetString("tables").Split(',');
            foreach(string table in tables)
            {
                IniFileSection ifs = file.GetIniFileSection("Table_" + table);
                Table t = Table.CreateFrom(ifs);

                string tableName = t.GetName();
                string columnString = ifs.GetString("columns");

                string[] columns = null;
                if(columnString!=null && columnString != "")
                {
                    columns = columnString.Split(',');
                }
                if (columns != null&& columns.Length>0)
                {
                    foreach (string column in columns)
                    {
                        IniFileSection colIfs = file.GetIniFileSection(tableName + "_" + column);
                        Column c = Column.CreateFrom(colIfs);
                        t.AddColumn(c);
                    }
                }

                result.AddTable(t);
            }
            return result;
        }
    }
}
