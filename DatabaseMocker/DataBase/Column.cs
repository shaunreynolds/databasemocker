﻿using Common.Ini;
using Common.Utilities;
using DatabaseMocker.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Database
{
    /// <summary>
    /// Represents a column of a database table. Contains a dataset of data generated from the AbstractGenerator given to it.
    /// </summary>
    public class Column
    {
        private AbstractGenerator generator;
        private readonly List<string> dataset = new List<string>();
        private string name;
        private bool isPrimaryKey;
        private Table foreignTable;
        private Column foreignColumn;

        public Column(string name, AbstractGenerator ag, bool isPrimaryKey=false)
        {
            this.name = name;
            this.generator = ag;
            this.isPrimaryKey = isPrimaryKey;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public AbstractGenerator GetGenerator()
        {
            return generator;
        }

        public void SetGenerator(AbstractGenerator ag)
        {

            this.generator = ag;
        }

        public void SetPrimaryKey(bool isPrimaryKey)
        {
            this.isPrimaryKey = isPrimaryKey;
        }

        public static Column CreateFrom(IniFileSection ifs)
        {
            string name = ifs.GetString("name");
            string genName = ifs.GetString("generator");
            bool isPrimary = ifs.GetBool("isPrimaryKey");
            AbstractGenerator ag = Generators.Generators.GetGeneratorByName(genName);
            GeneratorOptions go = GeneratorOptions.FromString(ifs.GetString("options"));
            ag.SetGeneratorOptions(go);

            Column result = new Column(name, ag, isPrimary);
            if (ifs.ContainsKey("foreignTable") && ifs.ContainsKey("foreignColumn"))
            {
                Table t = Tables.GetTable(ifs.GetString("foreignTable"));
                Column c = t.GetColumn(ifs.GetString("foreignColumn"));
                result.SetForeignKeyConstraint(t, c);
            }
            return result;
        }

        public IniFileSection GetIniFileSection()
        {
            IniFileSection ifs = new IniFileSection(name);
            ifs.SetKVP("name", name);
            ifs.SetKVP("generator", generator.GetName());
            ifs.SetKVP("isPrimaryKey", isPrimaryKey.ToString());
            ifs.SetKVP("options", generator.GetGeneratorOptions().ToString());
            try
            {
                ifs.SetKVP("foreignTable", foreignTable.GetName());
                ifs.SetKVP("foreignColumn", foreignColumn.GetName());
            }catch(Exception e)
            {
                Log.WriteLine($"Column {name} cannot save foreign key constraints as they are not set. This column most likely doesn't reference a foreign column.");
            }
            return ifs;
        }

        public void SetForeignKeyConstraint(Table t, Column c)
        {
            this.foreignColumn = c;
            this.foreignTable = t;
        }

        public bool IsPrimaryKey()
        {
            return isPrimaryKey;
        }

        public bool HasForeignKey()
        {
            return foreignTable != null && foreignColumn != null && generator.RequiresForeignKey();
        }

        public Table GetForeignTable()
        {
            return foreignTable;
        }

        public Column GetForeignColumn()
        {
            return foreignColumn;
        }

        public string GetName()
        {
            return name;
        }

        public string GetDataType()
        {
            return generator.GetDataType();
        }

        public bool IsGenerated()
        {
            return dataset.Count > 0;
        }

        public void Generate(int numItems)
        {
            if (!IsGenerated())
            {
                for (int i = 0; i < numItems; i++)
                {
                    dataset.Add(generator.GetData());
                }
            }
        }

        public string[] GetDataset()
        {
            return dataset.ToArray();
        }
    }
}
