﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Data
{
    public class URLs
    {
        public static readonly string MALE_NAMES = "http://kiralee.ddns.net/DBMocker/maleNames.txt";
        public static readonly string FEMALE_NAMES = "http://kiralee.ddns.net/DBMocker/femaleNames.txt";
        public static readonly string LAST_NAMES = "http://kiralee.ddns.net/DBMocker/lastNames.txt";
        public static readonly string STREET_NAMES = "http://kiralee.ddns.net/DBMocker/streetNames.txt";
        public static readonly string NOUNS = "http://kiralee.ddns.net/DBMocker/nouns.txt";
    }
}
