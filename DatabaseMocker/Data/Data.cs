﻿using Common.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseMocker.Data
{
    public class Data
    {
        public static readonly string[] STREET_NAME_SUFFIXES = {"Road","Avenue","Street","Lane" };
        public static readonly string[] EMAIL_PROVIDERS = {"gmail","outlook","msn","hotmail","aol","yahoo" };
        public static readonly string[] DOMAIN_EXTENSIONS = {"com","net","org","co.uk" };
        public static readonly Dictionary<string, string[]> ONLINE_LISTS = new Dictionary<string, string[]>();

        static Data()
        {
            ONLINE_LISTS.Add("female_names", StringManipulation.GetStringArrayFromURL(URLs.FEMALE_NAMES));
            ONLINE_LISTS.Add("male_names", StringManipulation.GetStringArrayFromURL(URLs.MALE_NAMES));
            ONLINE_LISTS.Add("last_names", StringManipulation.GetStringArrayFromURL(URLs.LAST_NAMES));
            ONLINE_LISTS.Add("street_names", StringManipulation.GetStringArrayFromURL(URLs.STREET_NAMES));
            ONLINE_LISTS.Add("nouns", StringManipulation.GetStringArrayFromURL(URLs.NOUNS));

            // Both male and female names
            string[] allNames = ArrayManipulation.ConcatArrays(ONLINE_LISTS["male_names"],ONLINE_LISTS["female_names"]);
            ONLINE_LISTS.Add("all_names", allNames);
        }
    }
}
