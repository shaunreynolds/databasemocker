﻿using Common.Ini;
using Common.Utilities;
using DatabaseMocker.Database;
using DatabaseMocker.Generators;
using DatabaseMocker.GUI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseMocker
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Log.AddTextWriter(new StreamWriter("C:\\Users\\Shaun\\Desktop\\log.txt"));
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
            Log.Dispose();
            Console.ReadLine();
        }

        static void Test3()
        {
            IniFile iF = new IniFile("C:\\Users\\Shaun\\Desktop\\columns.ini");
            Database.Database db = Database.Database.ReadFrom(iF);
            StreamWriter sw = new StreamWriter("C:\\Users\\Shaun\\Desktop\\output.sql");
            db.WriteOut(sw);
            sw.Flush();
            sw.Close();
            sw.Dispose();
        }

        static void Test2()
        {
            AbstractGenerator row = Generators.Generators.GetGeneratorByName("rowNumberGenerator");
            AbstractGenerator age = Generators.Generators.GetGeneratorByName("numberGenerator");
            AbstractGenerator name = Generators.Generators.GetGeneratorByName("listGenerator");
            AbstractGenerator email = Generators.Generators.GetGeneratorByName("emailGenerator");
            AbstractGenerator date = Generators.Generators.GetGeneratorByName("dateTimeGenerator");
            AbstractGenerator dataColumn = Generators.Generators.GetGeneratorByName("columnDataGenerator");

            GeneratorOptions go = new GeneratorOptions();
            GeneratorOptions go2 = new GeneratorOptions();
            go.AddGeneratorOption("min", "Min age: ", 10);
            go.AddGeneratorOption("max", "Max age: ", 25);
            go.AddGeneratorOption("decimalPlaces", "Decimal Places", 0);
            go.AddGeneratorOption("listName", "List of names: " ,"all_names");
            go.AddGeneratorOption("table", "table", "Person");
            go.AddGeneratorOption("column", "column", "id");

            go2.AddGeneratorOption("min", "Min", new DateTime(1991, 2, 14));
            go2.AddGeneratorOption("max", "Max", DateTime.Today);

            age.SetGeneratorOptions(go);
            name.SetGeneratorOptions(go);
            email.SetGeneratorOptions(go);
            date.SetGeneratorOptions(go2);

            dataColumn.SetGeneratorOptions(go);

            Column rowCol = new Column("id", row, true);
            Column ageCol = new Column("age", age);
            Column nameCol = new Column("name", name);
            Column emailCol = new Column("email", email);
            Column dobCol = new Column("DOB", date);

            Column email2Col = new Column("FKEmail", dataColumn);

            Table personTable = new Table("Person", 5);
            Table testTable2 = new Table("Test", 50);

            email2Col.SetForeignKeyConstraint(personTable, rowCol);

            Tables.AddTable(personTable);
            Tables.AddTable(testTable2);

            personTable.AddColumns(rowCol,ageCol, nameCol, emailCol,dobCol);
            testTable2.AddColumns(new Column("fkID",Generators.Generators.GetGeneratorByName("rowNumberGenerator"),true),email2Col);

            foreach(string s in personTable.GetCreateStatement())
            {
                Log.WriteLine(s);
            }
            foreach(string s in personTable.GetInsertStatements())
            {
                Log.WriteLine(s);
            }

            foreach (string s in testTable2.GetCreateStatement())
            {
                Log.WriteLine(s);
            }
            foreach (string s in testTable2.GetInsertStatements())
            {
                Log.WriteLine(s);
            }

            IniFile iF = new IniFile("C:\\Users\\Shaun\\Desktop\\columns.ini");
            Database.Database db = new Database.Database("TestDB");
            db.AddTables(personTable, testTable2);
            db.WriteTo(iF);
        }

        public static void FormatTextFile()
        {
            string fileLocation = "C:\\Users\\Shaun\\Desktop\\streetNames.txt";
            string outFileLocation = "C:\\Users\\Shaun\\Desktop\\streetNamesCondensed.txt";

            StreamWriter sw = new StreamWriter(outFileLocation);
            StreamReader sr = new StreamReader(fileLocation);
            while (sr.Peek() != -1)
            {
                string line = sr.ReadLine();
                line = line.ToLower();
                sw.WriteLine(Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(line));
            }
            sw.Flush();
            sw.Close();
            sw.Dispose();

            sr.Close();
            sr.Dispose();
        }

        static void Test()
        {
            string goString = "decimalPlaces;Decimal Places: ;3|min;Minimum value: ;-100|max;Maximum value: ;100|listName;Which list to pull from: ;all_names";
            GeneratorOptions go = GeneratorOptions.FromString(goString);
            GeneratorOptions go2 = new GeneratorOptions();
            go2.AddGeneratorOption("listName", "Which list to pull from: ", "last_names");
            go.AddGeneratorOption("decimalPlaces", "Decimal Places: ", 0);
            go.AddGeneratorOption("min", "Minimum value: ", 0);
            go.AddGeneratorOption("max", "Maximum value: ", 100);

            go.AddGeneratorOption("listName", "Which list to pull from: ", "street_names");
            GeneratorOptions go3 = new GeneratorOptions();
            go3.AddGeneratorOption("listName", "", "all_names");

            AbstractGenerator ng = new ListGenerator();
            AbstractGenerator ng2 = new ListGenerator();
            AbstractGenerator ng3 = new ListGenerator();
            AbstractGenerator email = new EmailGenerator();
            AbstractGenerator post = new PostcodeGenerator();
            AbstractGenerator numberGenerator = new NumberGenerator();
            numberGenerator.SetGeneratorOptions(go);
            ng3.SetGeneratorOptions(go);
            ng.SetGeneratorOptions(go3);
            ng2.SetGeneratorOptions(go2);

            for(int i = 0; i < 10; i++)
            {
                Log.WriteLine(ng.GetDataType() + " : " + ng.GetData()+" "+ng2.GetData()+", "+numberGenerator.GetData()+" "+ng3.GetData()+ " "+post.GetData()+", "+email.GetData());
            }

            Log.WriteLine(go);
            Log.WriteLine(go2);
        }
    }
}
