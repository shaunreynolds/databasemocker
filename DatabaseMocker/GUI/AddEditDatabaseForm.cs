﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseMocker.GUI
{
    public partial class AddEditDatabaseForm : Form
    {
        private bool isEditing = false;
        private Database.Database database;
        public AddEditDatabaseForm()
        {
            InitializeComponent();
        }
        public void SetDatabase(Database.Database database)
        {
            this.database = database;
            this.isEditing = true;
            this.nameTextBox.Text = database.GetName();
        }

        public Database.Database GetDatabase()
        {
            return database;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (isEditing)
            {
                database.SetName(nameTextBox.Text);
            }
            else
            {
                database = new Database.Database(nameTextBox.Text);
            }

            DialogResult = DialogResult.OK;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }
    }
}
