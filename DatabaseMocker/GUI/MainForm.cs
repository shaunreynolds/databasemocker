﻿using Common.Ini;
using Common.Utilities;
using DatabaseMocker.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseMocker.GUI
{
    public partial class MainForm : Form
    {
        Database.Database database;
        TreeNode selectedNode = null;
        string fileName = "";
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            this.treeView1.NodeMouseClick += TreeView1_NodeMouseClick;
            this.addButton.Enabled = false;
            this.editButton.Enabled = false;
        }

        private void TreeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            addButton.Enabled = ((string)e.Node.Tag != "columnNode");
            editButton.Enabled = true;
            deleteButton.Enabled = true;
            selectedNode = e.Node;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "DatabaseMocker files (*.dbm)|*.dbm";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string file = ofd.FileName;
                fileName = ofd.FileName;
                try
                {
                    database = Database.Database.ReadFrom(new IniFile(file));
                    EnableOptions();
                    PopulateTreeView();
                }catch(Exception exc)
                {
                    Log.WriteLine("Cannot create database from file: " + file);
                    Log.WriteLine(exc);
                }
            }
        }

        private void EnableOptions()
        {
            saveToolStripMenuItem.Enabled = true;
            saveAsToolStripMenuItem.Enabled = true;
            exportToolStripMenuItem.Enabled = true;
        }

        private void PopulateTreeView()
        {
            treeView1.Nodes.Clear();
            TreeNode databaseNode = new TreeNode(database.GetName());
            databaseNode.Tag = "databaseNode";
            foreach(Table t in database.GetTables())
            {
                TreeNode tableNode = new TreeNode(t.GetName());
                tableNode.Tag = "tableNode";
                databaseNode.Nodes.Add(tableNode);
                foreach(Column c in t.GetColumns())
                {
                    TreeNode columnNode = new TreeNode(c.GetName());
                    columnNode.Tag = "columnNode";
                    tableNode.Nodes.Add(columnNode);
                }
            }
            treeView1.Nodes.Add(databaseNode);
            treeView1.ExpandAll();
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "SQL file (*.sql)|*.sql|All files (*.*)|*.*";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string file = sfd.FileName;
                StreamWriter sw = new StreamWriter(file);
                database.WriteOut(sw);
                sw.Flush();
                sw.Close();
                sw.Dispose();
            }
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            if(selectedNode!=null)
            {
                string nodeType = (string)selectedNode.Tag;
                switch (nodeType)
                {
                    case "databaseNode":
                        string tableName = selectedNode.Text;
                        AddEditTableForm aetf = new AddEditTableForm();
                        if (aetf.ShowDialog() == DialogResult.OK)
                        {
                            database.AddTable(aetf.GetTable());
                            Tables.AddTable(aetf.GetTable());
                        }
                        break;
                    case "tableNode":
                        string tableName2 = selectedNode.Text;
                        Log.WriteLine("Node parent: " + selectedNode.Parent);
                        Log.WriteLine($"Attempting to find table: {tableName2}");
                        AddEditColumnForm aecf = new AddEditColumnForm();
                        if (aecf.ShowDialog() == DialogResult.OK)
                        {
                            Tables.GetTable(tableName2).AddColumn(aecf.GetColumn());
                        }
                        break;
                    case "columnNode":
                       
                        break;
                }
            }
            PopulateTreeView();
        }

        private void editButton_Click(object sender, EventArgs e)
        {
            if (selectedNode != null)
            {
                string nodeType = (string)selectedNode.Tag;
                switch (nodeType)
                {
                    case "databaseNode":
                        AddEditDatabaseForm aedf = new AddEditDatabaseForm();
                        aedf.SetDatabase(database);
                        if (aedf.ShowDialog() == DialogResult.OK)
                        {
                            database = aedf.GetDatabase();
                        }
                        break;
                    case "tableNode":
                        string tableName = selectedNode.Text;
                        Table t = database.GetTable(tableName);
                        AddEditTableForm aetf = new AddEditTableForm();
                        aetf.SetTable(t);
                        if (aetf.ShowDialog() == DialogResult.OK)
                        {
                            database.AddTable(aetf.GetTable());
                            Tables.AddTable(aetf.GetTable());
                        }
                        break;
                    case "columnNode":
                        string columnName = selectedNode.Text;
                        string tableName2 = selectedNode.Parent.Text;
                        Log.WriteLine("Node parent: " + selectedNode.Parent);
                        Log.WriteLine($"Attempting to find table: {tableName2} and column {columnName}");
                        Table t2 = Tables.GetTable(tableName2);
                        Column c = t2.GetColumn(columnName);
                        AddEditColumnForm aecf = new AddEditColumnForm();
                        aecf.SetColumn(c);
                        aecf.ShowDialog();
                        break;
                }
            }
            PopulateTreeView();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            IniFile iniFile = new IniFile(fileName);
            database.WriteTo(iniFile);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "DatabaseMocker files (*.dbm)|*.dbm";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                IniFile iniFile = new IniFile(sfd.FileName);
                database.WriteTo(iniFile);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            if (selectedNode != null)
            {
                string nodeType = (string)selectedNode.Tag;
                switch (nodeType)
                {
                    case "databaseNode":break;
                    case "tableNode":
                        string name = selectedNode.Text;
                        database.RemoveTable(name);
                        break;
                    case "columnNode":
                        string tableName = selectedNode.Parent.Text;
                        string columnName = selectedNode.Text;
                        Tables.GetTable(tableName).RemoveColumn(columnName);
                        break;
                }
            }
            PopulateTreeView();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (database != null)
            {
                DialogResult dr  = MessageBox.Show("Save", "Would you like to save the current database before creating a new one?",MessageBoxButtons.OKCancel);
                if (dr == DialogResult.OK)
                {
                    saveToolStripMenuItem_Click(null,null);
                }
            }
            AddEditDatabaseForm aedf = new AddEditDatabaseForm();
            if (aedf.ShowDialog() == DialogResult.OK)
            {
                database = aedf.GetDatabase();
                EnableOptions();
                PopulateTreeView();
            }
        }
    }
}
