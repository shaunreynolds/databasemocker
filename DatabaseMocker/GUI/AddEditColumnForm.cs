﻿using Common.Utilities;
using DatabaseMocker.Database;
using DatabaseMocker.Generators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseMocker.GUI
{
    public partial class AddEditColumnForm : Form
    {
        private bool isEditing = false;
        private Column column = null;
        public AddEditColumnForm()
        {
            InitializeComponent();
        }
        public void SetColumn(Column column)
        {
            this.column = column;
            this.isEditing = true;
        }

        public Column GetColumn()
        {
            return column;
        }

        private void AddEditColumnForm_Load(object sender, EventArgs e)
        {
            //generatorComboBox.DataSource = Generators.Generators.GetGenerators();
            foreach(AbstractGenerator ag in Generators.Generators.GetGenerators())
            {
                AbstractGenerator newAg = Generators.Generators.GetGeneratorByName(ag.GetName());
                generatorComboBox.Items.Add(newAg);
            }
            //generatorComboBox.SelectionChangeCommitted += GeneratorComboBox_SelectedValueChanged;
            generatorComboBox.SelectedIndexChanged += GeneratorComboBox_SelectedIndexChanged;
            if (isEditing)
            {
                nameTextbox.Text = column.GetName();
                primaryKeyCheckBox.Checked = column.IsPrimaryKey();
                generatorComboBox.SelectedIndex = generatorComboBox.FindString(column.GetGenerator().GetHumanReadableName());

                Log.WriteLine($"Are generator options empty? Shouldn't be: {column.GetGenerator().GetGeneratorOptions().IsEmpty()}");

                PopulateOptions(column.GetGenerator());
            }
        }

        private void GeneratorComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Log.WriteLine("Index changed, calling value changed ...");
            GeneratorComboBox_SelectedValueChanged(sender, e);
        }

        private void GeneratorComboBox_SelectedValueChanged(object sender, EventArgs e)
        {
            Log.WriteLine("Selected generator: " + generatorComboBox.SelectedItem);
            //AbstractGenerator temp =(AbstractGenerator) generatorComboBox.SelectedValue;
            AbstractGenerator name = (AbstractGenerator)generatorComboBox.SelectedItem;
            AbstractGenerator newTemp = Generators.Generators.GetGeneratorByName(name.GetName());
            PopulateOptions(newTemp);
        }

        public void PopulateOptions(AbstractGenerator ag)
        {
            Log.WriteLine("Trying to clear controls");
            layoutPanel.Controls.Clear();
            Log.WriteLine("Number of controls left: " + layoutPanel.Controls.Count);
            Log.WriteLine("Number of generator options - should be 0 for post code generator : " + ag.GetGeneratorOptions().GetAllOptions().Count());
            foreach (GeneratorOptions.GeneratorOption go in ag.GetGeneratorOptions().GetAllOptions())
            {
                Label label = new Label();
                label.Text = go.GetLabel();
                label.Anchor = AnchorStyles.Left;
                label.AutoSize = true;
                label.Name = go.GetName() + "label";

                TextBox value = new TextBox();
                value.Text = go.GetValue().ToString();
                value.Anchor = AnchorStyles.Left;
                value.Name = go.GetName() + "textbox";
                value.Enabled = go.IsEditable();

                Log.WriteLine($"Attempting to add the a label {label} and textbox {value} to layout panel");

                layoutPanel.Controls.Add(label);
                layoutPanel.Controls.Add(value);
                layoutPanel.SetFlowBreak(value, true);
            }
            layoutPanel.Update();
            ag = null;
        }

        private void exitButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            string name = nameTextbox.Text;
            AbstractGenerator ag = (AbstractGenerator) generatorComboBox.SelectedItem;
            AbstractGenerator gen = Generators.Generators.GetGeneratorByName(ag.GetName());
            bool isPrimaryKey = primaryKeyCheckBox.Checked;

            if (isEditing)
            {
                column.SetName(name);
                column.SetGenerator(gen);
                column.SetPrimaryKey(isPrimaryKey);
            }
            else
            {
                column = new Column(name, gen, isPrimaryKey);
            }

            // TODO : replace with actual code to scrape go
            GeneratorOptions go = column.GetGenerator().GetGeneratorOptions();
            foreach(GeneratorOptions.GeneratorOption option in go.GetAllOptions())
            {
                string optionName = option.GetName();
                Label label = layoutPanel.Controls.Find(optionName+"label", true).FirstOrDefault() as Label;
                TextBox textbox = layoutPanel.Controls.Find(optionName + "textbox", true).FirstOrDefault() as TextBox;

                Log.WriteLine($"Found appropriate label: {label} and textbox: {textbox}");
                go.AddGeneratorOption(optionName, label.Text, textbox.Text);
            }
            gen.SetGeneratorOptions(go);
            if (go.ContainsKey("table") && go.ContainsKey("column"))
            {
                column.SetForeignKeyConstraint(Tables.GetTable(go.GetString("table")), Tables.GetTable(go.GetString("table")).GetColumn(go.GetString("column")));
            }

            
            DialogResult = DialogResult.OK;
        }
    }
}
