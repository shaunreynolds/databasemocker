﻿using DatabaseMocker.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatabaseMocker.GUI
{
    public partial class AddEditTableForm : Form
    {
        private bool isEditing = false;
        private Table table = null;
        public void SetTable(Table t)
        {
            this.table = t;
            this.isEditing = true;
        }
        public AddEditTableForm()
        {
            InitializeComponent();
        }

        public Table GetTable()
        {
            return table;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (isEditing)
            {
                table.SetName(nameTextbox.Text);
                table.SetNumRows(int.Parse(numRowsTextbox.Text));
            }
            else
            {
                table = new Table(nameTextbox.Text, int.Parse(numRowsTextbox.Text));
            }
            DialogResult = DialogResult.OK;
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void AddEditTableForm_Load(object sender, EventArgs e)
        {
            if (isEditing)
            {
                this.nameTextbox.Text = table.GetName();
                this.numRowsTextbox.Text = table.GetNumRows().ToString();
            }
        }
    }
}
